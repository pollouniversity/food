package food.goodteam.com.tw;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.http.AndroidHttpClient;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class MenusActivity extends Activity {
	private ListView lview;
	private MyHandler handler;
	private ImageView shooping_car;
	private String jsonStr,s_title,cfg;
	private String[] mtitle,mcost,menudescription,menunumber,m_id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menus);
		
		lview = (ListView)findViewById(R.id.list_result);
		shooping_car = (ImageView)findViewById(R.id.shooping_car);
		
		Intent intent = getIntent();
		cfg = intent.getStringExtra("s_id");
		s_title = intent.getStringExtra("s_title");
		
		handler = new MyHandler();
		new GetHttpImage("http://192.168.1.105:8080/getYourData?table=2&cfg="+cfg+"").start();
		
		lview.setOnItemClickListener(new OnItemClickListener(){
				public void onItemClick(AdapterView<?> aView,View view, int index, long arg3){
					Toast.makeText(MenusActivity.this,mtitle[index] , Toast.LENGTH_LONG).show();
					gotoMenu(mtitle[index],mcost[index],menudescription[index],menunumber[index],Integer.toString(index));
				} 
		});
		
		shooping_car.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				gotoCheckout(mtitle,menunumber,mcost,m_id,s_title,cfg);
			}
		});
		
	}
	
	
	private class GetHttpImage extends Thread {
		private String url;
		public GetHttpImage(String u) {url = u;}
		public void run(){
			AndroidHttpClient client = AndroidHttpClient.newInstance("up to u");
			HttpGet get = new HttpGet(url);
			try {
				HttpResponse response = client.execute(get);
				InputStream is = response.getEntity().getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				StringBuilder out = new StringBuilder();
		        String line;
		        while ((line = reader.readLine()) != null) {
		            out.append(line);
		        }
				jsonStr = out.toString();
				Log.e("1",jsonStr);
				handler.sendEmptyMessage(1);			
			} catch (IOException e) {
			}
			client.close();
		}
	}
	
	private class MyHandler extends Handler{
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				try {
					
					JSONArray jsonobj = new JSONArray(jsonStr);
					ArrayList<HashMap<String,Object>> data;
					int[] to = {R.id.item_txt,R.id.item_desc};
					String[] from = {"item_txt","item_desc"};
					String[] m_title = new String[jsonobj.length()];
					String[] m_cost = new String[jsonobj.length()];
					String[] m_number = new String[jsonobj.length()];
					String[] menu_description = new String[jsonobj.length()];
					String[] mId = new String[jsonobj.length()];
					 
					//makers = new String[jsonobj.length()];
					int i=0;
					while(i<jsonobj.length()){
					
						menu_description[i] = null; 
						
						JSONObject lib = jsonobj.getJSONObject(i);
						m_title[i] = lib.getString("m_title");
						m_cost[i] = lib.getString("costs");
						mId[i] = lib.getString("m_id");
						m_number[i] = "0";
						menu_description[i] = lib.getString("menu_description");
					
		    			Log.e("store",m_title[i]);
		    			Log.e("store",m_cost[i]);
		    			Log.e("store",mId[i]);
		    			Log.e("store",menu_description[i]);
		    			i++;
					}
					
					data = new ArrayList<HashMap<String,Object>>();
					for(int j=0 ; j<m_title.length ; j++){
						HashMap<String, Object> temp = new HashMap<String,Object>();
						temp.put(from[0], m_title[j]);
						temp.put(from[1], m_cost[j]);
						data.add(temp);
					}
					SimpleAdapter adapter = new SimpleAdapter(MenusActivity.this, data, R.layout.menu_list, from, to);
					 
					lview.setAdapter(adapter);
					mtitle = m_title;
					mcost = m_cost;
					menudescription = menu_description;
					menunumber = m_number;
					m_id = mId;
				} catch (JSONException e) {
					e.printStackTrace();
				}
			break;
				
			
			} 
			
		}
	
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
		case 0:{
			Log.e("message","work_onActivityResult_0");
			if(resultCode == RESULT_OK){
				int number = Integer.valueOf(data.getStringExtra("number"));
				menunumber[number] = data.getStringExtra("m_number");
			}
		break;
		}
		case 1:{
			if(resultCode == RESULT_OK){
				String s_id = data.getStringExtra("s_id");
				String[] m_id = data.getStringArrayExtra("m_id");
				String[] m_number = data.getStringArrayExtra("m_number");
				gotoSent(s_id,m_id,m_number);
			}
		break;
		}
		}
	}
	
	private void gotoMenu(String m_title,String m_cost,String menu_description,String m_number,String number){
		Intent intent = new Intent(this, MenuActivity.class);
		intent.putExtra("m_title", m_title);
		intent.putExtra("m_cost", m_cost);
		intent.putExtra("menu_description", menu_description);
		intent.putExtra("m_number", m_number);
		intent.putExtra("number", number);
		//startActivity(intent);
		startActivityForResult(intent, 0);
	}
	
	private void gotoCheckout(String[] to_m_title, String[] to_m_number, String[] to_m_cost,String[] to_m_id,String to_s_title,String to_s_id){
		Intent intent = new Intent(this, CheckoutActivity.class);
		
		intent.putExtra("m_id", to_m_id);
		intent.putExtra("m_title", to_m_title);
		intent.putExtra("m_number", to_m_number);
		intent.putExtra("m_cost", to_m_cost);
		intent.putExtra("s_title", to_s_title);
		intent.putExtra("s_id", to_s_id);
		
		//startActivity(intent);
		
		startActivityForResult(intent, 1);
		super.onPause();
	}
	
	private void gotoSent(String s_id,String[] m_id,String[] m_number){
		Intent intent = new Intent();
		
		SharedPreferences personalFile = getSharedPreferences("personalFile", MODE_PRIVATE);
		String e_mail = personalFile.getString("e_mail", null);
		if(e_mail==null){
			intent.setClass(this,LoginActivity.class);
		}
		else{
			intent.setClass(this,SentActivity.class);
		}
		intent.putExtra("s_id", s_id);
		intent.putExtra("m_id", m_id);
		intent.putExtra("m_number", m_number);
		
		startActivity(intent);
		super.finish();
	}
}
