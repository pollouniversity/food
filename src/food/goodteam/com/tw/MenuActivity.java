package food.goodteam.com.tw;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewDebug.IntToString;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuActivity extends Activity {

	private ImageView add,reducing;
	private TextView text,m_title,costs,menu_derscription;
	private int m_number,number;
	private String mtitle,mcost,menudescription;
	private Button chick;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		add = (ImageView)findViewById(R.id.add);
		reducing = (ImageView)findViewById(R.id.reducing);
		m_title = (TextView)findViewById(R.id.m_title);
		costs = (TextView)findViewById(R.id.costs);
		menu_derscription = (TextView)findViewById(R.id.menu_derscription);
		text = (TextView)findViewById(R.id.text);
		chick = (Button)findViewById(R.id.chick);
		
		Intent intent = getIntent();
		mtitle = intent.getStringExtra("m_title");
		mcost = intent.getStringExtra("m_cost");
		menudescription = intent.getStringExtra("menu_description");
		m_number = Integer.valueOf(intent.getStringExtra("m_number"));
		number = Integer.valueOf(intent.getStringExtra("number"));
		
		m_title.setText(mtitle);
		costs.setText(mcost);
		menu_derscription.setText(menudescription);
		text.setText(Integer.toString(m_number));
		
		add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				m_number = m_number+1;
				text.setText(Integer.toString(m_number));
			}
		});
		reducing.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(m_number>0){
					m_number = m_number-1;
					text.setText(Integer.toString(m_number));
				}
				
			}
		});
		
		chick.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				chick(Integer.toString(m_number),Integer.toString(number));
				
			}
		});
	}
	
	private void chick(String m_number,String number){
		Intent intent = new Intent();
		intent.putExtra("m_number", m_number);
		intent.putExtra("number", number);
		setResult(RESULT_OK,intent);
		
		super.finish();
	}

}
