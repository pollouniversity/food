package food.goodteam.com.tw;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class CustomerRecordActivity extends Activity {

	private ListView lview;
	private ImageView customer_record_btn_previou;
	private MyHandler handler;
	private String[] array_data_cdate,data_title,data_id_number;
	private String data_string;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_customer_record);
		
		lview = (ListView)findViewById(R.id.list_customer_record);
		customer_record_btn_previou = (ImageView)findViewById(R.id.customer_record_btn_previou);
		
		handler = new MyHandler();
		
		SharedPreferences personalFile = getSharedPreferences("personalFile", MODE_PRIVATE);
		
		String e_mail = null;
		e_mail = personalFile.getString("e_mail", null);
		if(e_mail!=null){
			Intent intent = new Intent(this,Customer.class);
			intent.putExtra("do_case", "3");
			intent.putExtra("e_mail", e_mail);
			startActivityForResult(intent, 0);
		}
		
		customer_record_btn_previou.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
		case 0:{
			if(resultCode == RESULT_OK){
				int do_case = Integer.valueOf(data.getStringExtra("do_case"));
				switch(do_case){
				case 1:{
					data_string = data.getStringExtra("message");
					handler.sendEmptyMessage(1);
				break;
				}
				case 3:{
					Toast.makeText(this, "請檢查網路連線", Toast.LENGTH_LONG).show();
				break;
				}
				}
			}	
		break;
		}
			}
		}
	private class MyHandler extends Handler{
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:{
				try {
					JSONArray jsonobj = new JSONArray(data_string);
					Log.e("data_string",data_string);
					ArrayList<HashMap<String,Object>> data;
					
					int[] to = {R.id.id_number_txt,R.id.title_txt,R.id.date_txt};
					String[] from = {"id_number_txt","title_txt","date_txt"};
					
					String[] id_number = new String[jsonobj.length()];
					String[] title = new String[jsonobj.length()];
					String[] cdate = new String[jsonobj.length()];
					
					int i=0;
					while(i<jsonobj.length()){
						JSONObject lib = jsonobj.getJSONObject(i);
						id_number[i] = lib.getString("id_number");
						title[i] = lib.getString("title");
						cdate[i] = lib.getString("cdate");
						i++;
					}
					
					data = new ArrayList<HashMap<String,Object>>();
					for(int j=0 ; j<id_number.length ; j++){
						HashMap<String, Object> temp = new HashMap<String,Object>();
						temp.put(from[0], id_number[j]);
						temp.put(from[1], title[j]);
						temp.put(from[2], cdate[j]);
						data.add(temp);
					}
					
					SimpleAdapter adapter = new SimpleAdapter(CustomerRecordActivity.this, data, R.layout.record_list, from, to);
					lview.setAdapter(adapter);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					Log.e("mesage",ex.toString());
				}
			break;
			}
			}
		}
	}
}
