package food.goodteam.com.tw;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class CheckoutActivity extends Activity {

	private Button sentmenu;
	private ListView lview;
	private MyHandler handler;
	private String s_title,from_s_id;
	private String[] from_m_title,from_m_cost,from_m_number,from_m_id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_checkout);
		
		lview = (ListView)findViewById(R.id.list_check);		
		sentmenu = (Button)findViewById(R.id.sentmenu);
		
		handler = new MyHandler();
		//new GetIntent().start();
		
		Intent intent = getIntent();
		s_title = intent.getStringExtra("s_title");
		Log.e("s_title",s_title);
		from_s_id = intent.getStringExtra("s_id");
		from_m_id = intent.getStringArrayExtra("m_id");
		from_m_title = intent.getStringArrayExtra("m_title");
		from_m_cost = intent.getStringArrayExtra("m_cost");
		from_m_number = intent.getStringArrayExtra("m_number");
	
		sentmenu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
					gotoSentActivity(from_s_id,from_m_id,from_m_number);
			}
		});
		handler.sendEmptyMessage(1);
		
	}

	

	
	private class MyHandler extends Handler{
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:

					ArrayList<HashMap<String,Object>> data;
					int[] to = {R.id.m_name,R.id.menu_amount,R.id.doller};
					String[] from = {"m_name","menu_amount","doller"};

					data = new ArrayList<HashMap<String,Object>>();
					for(int i=0 ; i<from_m_title.length ; i++){
						if(Integer.valueOf(from_m_number[i])!=0){
							HashMap<String, Object> temp = new HashMap<String,Object>();
							temp.put(from[0], from_m_title[i]);
							Log.e("m_title"+i,from_m_title[i]);
							temp.put(from[1], from_m_cost[i]);
							Log.e("m_cost"+i,from_m_cost[i]);
							temp.put(from[2], "��");
							data.add(temp);
						}
					}
					SimpleAdapter adapter = new SimpleAdapter(CheckoutActivity.this, data, R.layout.checkout_list, from, to);
					
					lview.setAdapter(adapter);
			break;
			} 
		}
	}

	
	private void gotoSentActivity(String s_id,String[] m_id,String[] m_number){
		
		Intent intent = new Intent();
		
		SharedPreferences personalFile = getSharedPreferences("personalFile", MODE_PRIVATE);
		String e_mail = personalFile.getString("e_mail", null);
		if(e_mail==null){
			intent.setClass(this,LoginActivity.class);
		}
		else{
			intent.setClass(this,SentActivity.class);
		}
		intent.putExtra("s_id", s_id);
		intent.putExtra("m_id", m_id);
		intent.putExtra("m_number", m_number);
		//startActivity(intent);
		setResult(RESULT_OK,intent);
		super.finish();
	}
}
