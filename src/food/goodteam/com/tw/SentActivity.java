package food.goodteam.com.tw;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SentActivity extends Activity {
	
	private String from_s_id,from_e_mail,from_adderss;
	private String[] from_m_id,from_m_number;
	private MyHandler handler;
	private TextView address;
	private Button return_index;
	
	
	@Override
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sent);
		
		address = (TextView)findViewById(R.id.address);
		return_index = (Button)findViewById(R.id.return_index);
		
		handler = new MyHandler();
		new GetHttp().start();
		
		return_index.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	private class GetHttp extends Thread{
		
		public GetHttp(){}
		public void run(){
			try{
				
				SharedPreferences personalFile = getSharedPreferences("personalFile", MODE_PRIVATE);
				
				String message = null;	
				Intent intent = getIntent();
				from_m_id = intent.getStringArrayExtra("m_id");
				from_m_number = intent.getStringArrayExtra("m_number");
				from_s_id = intent.getStringExtra("s_id");
				from_e_mail = personalFile.getString("e_mail", null);;
				from_adderss = personalFile.getString("address", null);;
				
				JSONArray jsonarray = new JSONArray();
				JSONObject jsonobject ;
				int i = 0;
				while(i<from_m_id.length){
					if(Integer.valueOf(from_m_number[i])!=0){
					jsonobject = new JSONObject();
					
					jsonobject.put("s_id",from_s_id);
					jsonobject.put("e_mail",from_e_mail);
					jsonobject.put("m_id",from_m_id[i]);
					jsonobject.put("m_number",from_m_number[i]);
					jsonobject.put("address",from_adderss);
					jsonobject.put("cancel","0");
					jsonarray.put(jsonobject);
					
					}
					i++;  
				} 
				Log.e("JSON",jsonarray.toString());
				 
				//Do Internetwork 
				DefaultHttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost("http://192.168.137.6:8080/getYourData");
				List<NameValuePair> params = new ArrayList<NameValuePair>(1);
				params.add(new BasicNameValuePair("userData",jsonarray.toString()));
				params.add(new BasicNameValuePair("do_case","1"));
				post.setEntity(new UrlEncodedFormEntity(params,HTTP.UTF_8));
				HttpResponse response = client.execute(post);
				message = EntityUtils.toString(response.getEntity());
				Log.e("message",message);
				if(message.equals("ok")){
					Log.e("message","hello!");
					handler.sendEmptyMessage(1);
				}else{
					
				}
			}catch(Exception d){
				Log.e("message",d.toString());
			}	
		}	
	} 
	
	private class MyHandler extends Handler{
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				address.setText(from_adderss);
			break;
			}
		}
	}
}
