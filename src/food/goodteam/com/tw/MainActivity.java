package food.goodteam.com.tw;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.http.AndroidHttpClient;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import food.goodteam.com.tw.MyLocation.LocationResult;

public class MainActivity extends Activity {
	
	private Button china,korea,nanyang,japan,american,drink;
	private Dialog dialog_newreg;
	private ImageView  people,search;
	private ImageButton latlng,maplist;
	private TextView infon;
	private MyHandler handler;
	private String lat,lng,jsonStr;
	private static final int TWO_MINUTES = 1000 * 10;
	private double nowLat, nowLng;
	private WebView webview;

	protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);
	
	webview = (WebView)findViewById(R.id.webview);
	maplist =(ImageButton)findViewById(R.id.maplist);
	people = (ImageView)findViewById(R.id.people);
	latlng = (ImageButton)findViewById(R.id.latlng);
	infon = (TextView)findViewById(R.id.infon);
	
	LocationResult locationResult = new LocationResult() {
		@Override
		public void gotLocation(Location location) {
			nowLat = location.getLatitude();
			nowLng = location.getLongitude();
			infon.setText(nowLat + " : " + nowLng);
			webview.loadUrl("javascript:initialize("+nowLat+","+nowLng+")");
		}
	};
	
	MyLocation myLocation = new MyLocation();
	myLocation.getLocation(this, locationResult);
	
	initWebView();
	
	handler = new MyHandler();
	
	//SelectButton
	maplist.setOnClickListener(MyListener);
	people.setOnClickListener(MyListener);
	latlng.setOnClickListener(MyListener);
	
	}
	
	private void initWebView() {
		webview.setWebViewClient(new WebViewClient());
		WebSettings settings = webview.getSettings();
		settings.setJavaScriptEnabled(true);//開放javascript權限
		webview.addJavascriptInterface(new FromJavaScript(this), "ToStore");//建立javascript與java互動的介面，指定要執行的java物件(MyJavaScript)與執行程式所在地(MainActivity.this)，設定介面名稱，讓javascript識別的字串"yypalert"。如此一來JavaScript可以經由yypalert存取.java物件的method。
		webview.loadUrl("file:///android_asset/map.html");//導至自訂的網頁
		webview.setVisibility(View.VISIBLE);
		
		//SelectButton
		maplist.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				showAlertDialog();
			}
		});
	}
	
	//接收訊息給javaSctipt
	private class FromJavaScript{
		 private Context context;//因為Toast要指定toast顯示地方，所以宣告context，接收上面的MainActivity.this物件
		 FromJavaScript(Context c){
		  context=c;
		 }
		 public void gotoStore(String msg, String s_title){
		  Toast.makeText(context, s_title,Toast.LENGTH_LONG).show();
		  gotoStoreActivity(msg,s_title);
		  onPause();
		 }
	}
	
	//Dialog
	private void showAlertDialog(){
		dialog_newreg = new Dialog(this, R.style.dialogStyle);
		dialog_newreg.setContentView(R.layout.newreg);
		
		Window dialogWindow = dialog_newreg.getWindow();
		WindowManager.LayoutParams lp = dialogWindow.getAttributes();
		//dialogWindow.setGravity(Gravity.BOTTOM | Gravity.RIGHT);
		lp.width = 300; // 寬度
		//lp.height = 100; // 高度
		lp.alpha = 0.7f; // 透明度
		
		dialog_newreg.setCancelable(false);
		
		search =(ImageView)dialog_newreg.findViewById(R.id.search);
		china =(Button)dialog_newreg.findViewById(R.id.china);
		korea =(Button)dialog_newreg.findViewById(R.id.korea);
		nanyang =(Button)dialog_newreg.findViewById(R.id.nanyang);
		japan =(Button)dialog_newreg.findViewById(R.id.japan);
		american =(Button)dialog_newreg.findViewById(R.id.american);
		drink =(Button)dialog_newreg.findViewById(R.id.drink);
		
		search.setOnClickListener(MyListener);
		china.setOnClickListener(MyListener);
		korea.setOnClickListener(MyListener);
		nanyang.setOnClickListener(MyListener);
		japan.setOnClickListener(MyListener);
		american.setOnClickListener(MyListener);
		drink.setOnClickListener(MyListener);
		
		dialog_newreg.show();
	}
	
	private View.OnClickListener MyListener = new View.OnClickListener(){
		public void onClick(View v){
			switch(v.getId()){
			case R.id.maplist:{
				showAlertDialog();
			break;
			}
			case R.id.people:{
				Intent intent = new Intent(MainActivity.this,PeopleIndexActivity.class);
				startActivity(intent);
			break;
			}
			case R.id.latlng:{
				webview.loadUrl("javascript:moveTo("+nowLat+","+nowLng+")");
			break;
			}
			
			// Dialog btn
			case R.id.search:{
				new GetHttpImage("http://192.168.1.105:8080/getYourData?table=1&cfg=0").start();
				dialog_newreg.dismiss();
			break;
			}
			case R.id.china:{
				new GetHttpImage("http://192.168.1.105:8080/getYourData?table=1&cfg=0").start();
				dialog_newreg.dismiss();
			break;
			}
			case R.id.korea:{
				new GetHttpImage("http://192.168.1.105:8080/getYourData?table=1&cfg=1").start();
				dialog_newreg.dismiss();
			break;
				}
			case R.id.nanyang:{
				new GetHttpImage("http://192.168.1.105:8080/getYourData?table=1&cfg=2").start();
				dialog_newreg.dismiss();
			break;
				}
			case R.id.japan:{
				new GetHttpImage("http://192.168.1.105:8080/getYourData?table=1&cfg=3").start();
				dialog_newreg.dismiss();
			break;
				}
			case R.id.american:{
				new GetHttpImage("http://192.168.1.105:8080/getYourData?table=1&cfg=4").start();
				dialog_newreg.dismiss();
			break;
				}
			case R.id.drink:{
				new GetHttpImage("http://192.168.1.105:8080/getYourData?table=1&cfg=5").start();
				dialog_newreg.dismiss();
			break;
				}
			}
		}
	};
			
	
	private class GetHttpImage extends Thread {
		private String url;
		public GetHttpImage(String u) {url = u;}
		public void run(){
			AndroidHttpClient client = AndroidHttpClient.newInstance("up to u");
			HttpGet get = new HttpGet(url);
			try {
				HttpResponse response = client.execute(get);
				InputStream is = response.getEntity().getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				StringBuilder out = new StringBuilder();
		        String line;
		        while ((line = reader.readLine()) != null) {
		            out.append(line);
		        }
				jsonStr = out.toString();
				Log.e("1",jsonStr);
				handler.sendEmptyMessage(1);
			} catch (IOException e) {
			}
			client.close();
		}
	}
    
	private class MyHandler extends Handler{
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				try {
					JSONArray jsonobj = new JSONArray(jsonStr);
					String[] title = new String[jsonobj.length()];
					String[] s_id = new String[jsonobj.length()];
					String[] jlat = new String[jsonobj.length()];
					String[] jlng = new String[jsonobj.length()];
					//makers = new String[jsonobj.length()];
					int i=0;
					while(i<jsonobj.length()){
						JSONObject lib = jsonobj.getJSONObject(i);
		    			title[i] = "'"+lib.getString("title")+"'";
		    			s_id[i] = "'"+lib.getString("s_id")+"'";
		    			jlat[i] = lib.getString("lat");
		    			jlng[i] = lib.getString("lng");
		    			Log.e("s_id",lib.getString("s_id"));
		    			Log.e("jtitle",title[i]);
		    			Log.e("lat",lib.getString("lat"));
		    			Log.e("lng",lib.getString("lng"));
		    			i++;
					}
					lat="25.058668";
					lng="121.54445299999998";
					
					webview.loadUrl("javascript:initialize("+lat+","+lng+")");
					i=0;
					while(i<jsonobj.length()){
						webview.loadUrl("javascript:setMarkers("+jlat[i]+","+jlng[i]+","+title[i]+","+s_id[i]+")");
						i++;
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} 
		}
	}
	
	private void gotoStoreActivity(String s_id, String s_title){
		Intent intent = new Intent(this,MenusActivity.class);
		intent.putExtra("s_id", s_id);
		intent.putExtra("s_title", s_title);
		startActivity(intent);
		onPause();
	}
	
	protected boolean isBetterLocation(Location location,
			Location currentBestLocation) {
		if (currentBestLocation == null) {
			// 預設以新抓到的位置為最佳位置物件
			return true;
		}

		// 檢查修正後新位置是否較新
		long timeDelta = location.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		boolean isNewer = timeDelta > 0;

		// 判斷是否距離上次位置時間為兩分鐘以上, 是的話就對了, 因為使用者應該是在移動狀態
		if (isSignificantlyNewer) {
			return true;
		} else if (isSignificantlyOlder) {
			return false;
		}

		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// 檢查兩個位置的取得是否相同的提供者
		boolean isFromSameProvider = isSameProvider(location.getProvider(),currentBestLocation.getProvider());
		if(isMoreAccurate) {
			return true;
		}
		else if (isNewer && !isLessAccurate) {
			return true;
		}
		else if (isNewer && !isSignificantlyLessAccurate
				&& isFromSameProvider) {
			return true;
		}
		return false;
	}

	private boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}
}	