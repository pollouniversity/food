package food.goodteam.com.tw;

import java.text.SimpleDateFormat;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CustomerSettingActivity extends Activity {
	
	private TextView dtitle_id_txt,dtitle_name_txt,dtitle_mdate_txt,dtitle_cdate_txt;
	private EditText dtitle_address_txt,dtitle_phone_txt;
	private Button setting_check_btn,setting_concel_btn;
	private String name,e_mail,address,phone,cdate,mdate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_customer_setting);

		dtitle_id_txt = (TextView)findViewById(R.id.dtitle_id_txt);
		dtitle_name_txt = (TextView)findViewById(R.id.dtitle_name_txt);
		dtitle_mdate_txt = (TextView)findViewById(R.id.dtitle_mdate_txt);
		dtitle_cdate_txt = (TextView)findViewById(R.id.dtitle_cdate_txt);
		dtitle_address_txt = (EditText)findViewById(R.id.dtitle_address_txt);
		dtitle_phone_txt = (EditText)findViewById(R.id.dtitle_phone_txt);
		setting_check_btn = (Button)findViewById(R.id.setting_check_btn);
		setting_concel_btn = (Button)findViewById(R.id.setting_concel_btn);
		
		SharedPreferences personalFile = getSharedPreferences("personalFile", MODE_PRIVATE);
		name = personalFile.getString("name", null);
		e_mail = personalFile.getString("e_mail", null);
		address = personalFile.getString("address", null);
		phone = personalFile.getString("phone", null);
		cdate = personalFile.getString("cdate", null);
		mdate = personalFile.getString("mdate", null);
		
		dtitle_id_txt.setText(e_mail);
		dtitle_name_txt.setText(name);
		dtitle_address_txt.setText(address);
		dtitle_phone_txt.setText(phone);
		dtitle_cdate_txt.setText(cdate);
		if(mdate!=null){
			dtitle_mdate_txt.setText(mdate);
		}
		else{
			dtitle_mdate_txt.setText("�L�ק����");	
		}
		
		setting_check_btn.setOnClickListener(MyListener);
		setting_concel_btn.setOnClickListener(MyListener);
	}
	
	private View.OnClickListener MyListener = new View.OnClickListener() {
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.setting_check_btn:{
				SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				address = dtitle_address_txt.getText().toString();
				phone = dtitle_phone_txt.getText().toString();
				mdate = sdFormat.format(new java.util.Date());
				
				SharedPreferences personalFile = getSharedPreferences("personalFile", MODE_PRIVATE);
				Editor editor = personalFile.edit();
				editor.putString("address", address);
				editor.putString("phone", phone);
				editor.putString("mdate", mdate);
				editor.commit();
				
				Intent intent = new Intent(CustomerSettingActivity.this,Customer.class);
				intent.putExtra("do_case", "4");
				intent.putExtra("e_mail", e_mail);
				startActivity(intent);
				finish();
			break;
			}
			case R.id.setting_concel_btn:{
				finish();
			break;
			}
			}
		}
	};
}
