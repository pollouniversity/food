package food.goodteam.com.tw;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class PeopleIndexActivity extends Activity {

	private RelativeLayout btn_favority_box,btn_customer_record_box,btn_customer_setting_box,btn_connentus_box,btn_logout_box,unlogin_view;
	private ScrollView login_view;
	private TextView people_index_info,title_e_mail_second,title_phone_second,title_address_second;
	private ImageView people_inedx_btn_previou;
	private String name,e_mail,address,phone;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_people_index);
		
		people_inedx_btn_previou = (ImageView)findViewById(R.id.people_inedx_btn_previou);
		
		login_view = (ScrollView)findViewById(R.id.login_view);
		
		unlogin_view = (RelativeLayout)findViewById(R.id.unlogin_view);
		btn_favority_box = (RelativeLayout)findViewById(R.id.btn_favority_box);
		btn_customer_record_box = (RelativeLayout)findViewById(R.id.btn_customer_record_box);
		btn_customer_setting_box = (RelativeLayout)findViewById(R.id.btn_customer_setting_box);
		btn_connentus_box = (RelativeLayout)findViewById(R.id.btn_connentus_box);
		btn_logout_box = (RelativeLayout)findViewById(R.id.btn_logout_box);
		
		people_index_info = (TextView)findViewById(R.id.people_index_info);
		title_e_mail_second = (TextView)findViewById(R.id.title_e_mail_second);
		title_phone_second = (TextView)findViewById(R.id.title_phone_second);
		title_address_second = (TextView)findViewById(R.id.title_address_second);
		
		SharedPreferences personalFile = getSharedPreferences("personalFile", MODE_PRIVATE);
		name = personalFile.getString("name", null);
		e_mail = personalFile.getString("e_mail", null);
		address = personalFile.getString("address", null);
		phone = personalFile.getString("phone", null);
		
		if(name!=null){
			login_view.setVisibility(View.VISIBLE);
			unlogin_view.setVisibility(View.INVISIBLE);
			people_index_info.setText(name);
			title_e_mail_second.setText(e_mail);
			title_phone_second.setText(phone);
			title_address_second.setText(address);
		}
		
		people_inedx_btn_previou.setOnClickListener(MyListener);
		btn_favority_box.setOnClickListener(MyListener);
		btn_customer_record_box.setOnClickListener(MyListener);
		btn_customer_setting_box.setOnClickListener(MyListener);
		btn_connentus_box.setOnClickListener(MyListener);
		btn_logout_box.setOnClickListener(MyListener);
		
	}
	
	
	private View.OnClickListener MyListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.people_inedx_btn_previou:
			{
				finish();
				break;
			}
			case R.id.btn_favority_box:
			{
				Intent intent = new Intent(PeopleIndexActivity.this,FavorityActivity.class);
				startActivity(intent);
				onPause();
				break;
			}
			case R.id.btn_customer_record_box:
			{
				Intent intent = new Intent(PeopleIndexActivity.this,CustomerRecordActivity.class);
				startActivity(intent);
				onPause();
				break;
			}case R.id.btn_customer_setting_box:
			{
				Intent intent = new Intent(PeopleIndexActivity.this,CustomerSettingActivity.class);
				startActivity(intent);
				onPause();
				break;
			}case R.id.btn_connentus_box:
			{
				Intent intent = new Intent(PeopleIndexActivity.this,ConnentUsActivity.class);
				startActivity(intent);
				onPause();
				break;
			}
			case R.id.btn_logout_box:
			{
				SharedPreferences personalFile = getSharedPreferences("personalFile", MODE_PRIVATE);
				//Editor editor = personalFile.edit();
				personalFile.edit().clear().commit();
				/*
				editor.remove("e_mail");
				editor.remove("address");
				editor.remove("name");
				editor.remove("phone");
				editor.remove("cdate");
				editor.remove("mdate");
				editor.commit();
				*/
				btn_logout_box.setVisibility(View.INVISIBLE);
				name = personalFile.getString("name", null);
				people_index_info.setText("�ӤH����");
				break;
			}
				
				

			default:
				break;
			}
		}
	};
}
