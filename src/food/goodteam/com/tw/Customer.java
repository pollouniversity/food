package food.goodteam.com.tw;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class Customer extends Activity {
	
	private MyHandler handler;
	private String message = null;
	private String e_mail,data_address,data_name,data_phone,data_cdate,data_mdate,data_pwd;
	private int do_case;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		handler = new MyHandler();
		new PostHttp().start();
	}
	
	private class PostHttp extends Thread{
		public PostHttp(){}
		public void run(){
			DefaultHttpClient client = new DefaultHttpClient(); //連線的類型
			HttpResponse response = null;
			HttpPost post = new HttpPost("http://192.168.1.105:8080/getYourData");
			try{
				Intent intent = getIntent();
				do_case = Integer.valueOf(intent.getStringExtra("do_case"));
				e_mail = intent.getStringExtra("e_mail");				
				
				List<NameValuePair> params = new ArrayList<NameValuePair>(1);
				
				
				switch(do_case){
				case 1:{ //登入流程
					params.add(new BasicNameValuePair("do_case","2"));
					params.add(new BasicNameValuePair("e_mail",e_mail));
					post.setEntity(new UrlEncodedFormEntity(params,HTTP.UTF_8));	
					response = client.execute(post);
					if(response!=null){
						message = EntityUtils.toString(response.getEntity());
						handler.sendEmptyMessage(1);
					}
					else {
						toDoActivity(4);
					}
					break;
				}
				case 2:{ // 註冊流程
					data_pwd = intent.getStringExtra("pwd");
					data_name = intent.getStringExtra("name");
					data_address = intent.getStringExtra("address");
					data_phone = intent.getStringExtra("phone");
					
					params.add(new BasicNameValuePair("do_case","3"));
					params.add(new BasicNameValuePair("pwd",data_pwd));
					params.add(new BasicNameValuePair("name",data_name));
					params.add(new BasicNameValuePair("address",data_address));
					params.add(new BasicNameValuePair("phone",data_phone));
					params.add(new BasicNameValuePair("e_mail",e_mail));
					
					post.setEntity(new UrlEncodedFormEntity(params,HTTP.UTF_8));
					response = client.execute(post);
					if(response!=null){
						message = EntityUtils.toString(response.getEntity());
						handler.sendEmptyMessage(2);
					}
					else {
						toDoActivity(4);
					}
					break;
				}
				case 3:{//顧客查詢訂餐紀錄
					params.add(new BasicNameValuePair("do_case","4"));
					params.add(new BasicNameValuePair("e_mail",e_mail));
					
					post.setEntity(new UrlEncodedFormEntity(params,HTTP.UTF_8));
					response = client.execute(post);
					if(response!=null){
						message = EntityUtils.toString(response.getEntity());
						Log.e("message_from_CustomerRecord",message);
					}
					else {
						toDoActivity(4);
					}
					handler.sendEmptyMessage(3);
					break;
				}
				case 4:{
					SharedPreferences personalFile = getSharedPreferences("personalFile", MODE_PRIVATE);
					Editor editor = personalFile.edit();
					data_phone = personalFile.getString("phone", "無資料");
					data_mdate = personalFile.getString("mdate", "無資料");
					
					params.add(new BasicNameValuePair("do_case","5"));
					params.add(new BasicNameValuePair("e_mail",e_mail));
					params.add(new BasicNameValuePair("phone",data_phone));
					params.add(new BasicNameValuePair("mdate",data_mdate));
					
					post.setEntity(new UrlEncodedFormEntity(params,HTTP.UTF_8));
					response = client.execute(post);
					if(response!=null){
						message = EntityUtils.toString(response.getEntity());
					}
					else {
						toDoActivity(4);
					}
					handler.sendEmptyMessage(4);
				break;
				}
				}	
			}
			catch(Exception d){
				Log.e("message",d.toString());
				toDoActivity(4);
			} 
			//client.clone();
		}
	}
	
	private class MyHandler extends Handler{
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:{
				if(message.equals("0")){
					toDoActivity(2);
				}
				else{
					try{
						JSONArray jsonobj = new JSONArray(message);
						String[] pwd = new String[jsonobj.length()];
						String[] name = new String[jsonobj.length()];
						String[] phone = new String[jsonobj.length()];
						String[] cdate = new String[jsonobj.length()];
						String[] mdate = new String[jsonobj.length()];
						int i=0;
						while(i<jsonobj.length()){
							JSONObject lib = jsonobj.getJSONObject(i);							
							pwd[i] = lib.getString("pwd");
							name[i] = lib.getString("name");
							phone[i] = lib.getString("phone");
							cdate[i] = lib.getString("cdate");
							mdate[i] = lib.getString("mdate");
							i++;
						}
						
						data_pwd = pwd[0];						
						data_name = name[0];
						data_phone = phone[0];
						data_cdate = cdate[0];
						data_mdate = mdate[0];
						
						toDoActivity(1);
					}
					catch(JSONException ex){
						Log.e("message",ex.toString());
					}
				}
				break;
			}
			case 2:{
				if(message.equals("1")){
					SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					data_cdate = sdFormat.format(new java.util.Date());
					data_mdate = "無資料";
					toDoActivity(3);
				}
				else{					
					toDoActivity(2);
				}
				
				break;
			}
			case 3:{
				if(message.equals("1")){
					toDoActivity(4);
				}
				else{	
					toDoActivity(5);
				}
				break;
			}
			case 4:{
				toDoActivity(6);
				break;
			}
			}
		}
	}
	
	private void toDoActivity(int go_where){
		switch(go_where){
		case 1:
			{
			Intent intent = new Intent();
			intent.putExtra("do_case", "1");
			intent.putExtra("pwd", data_pwd);
			intent.putExtra("e_mail", e_mail);
			intent.putExtra("name", data_name);
			intent.putExtra("phone", data_phone);
			intent.putExtra("cdate", data_cdate);
			intent.putExtra("mdate", data_mdate);
			setResult(RESULT_OK,intent);
			super.finish();
			break;
			}
		case 2:{
			Intent intent = new Intent();
			intent.putExtra("do_case", "2");
			setResult(RESULT_OK,intent);
			super.finish();
			break;
		}
		case 3:{
			Intent intent = new Intent();
			intent.putExtra("do_case", "1");
			intent.putExtra("address", data_address);
			intent.putExtra("name", data_name);
			intent.putExtra("phone", data_phone);
			intent.putExtra("cdate", data_cdate);
			intent.putExtra("mdate", data_mdate);
			setResult(RESULT_OK,intent);
			super.finish();
			break;
		}
		case 4:{//失敗
			Intent intent = new Intent();
			intent.putExtra("do_case", "3");
			setResult(RESULT_OK,intent);
			super.finish();
		break;
		}
		case 5:{
			Intent intent = new Intent();
			intent.putExtra("do_case", "1");
			intent.putExtra("message", message);
			setResult(RESULT_OK,intent);
			super.finish();
		break;
		}
		case 6:{
			Intent intent = new Intent(this,PeopleIndexActivity.class);
			startActivity(intent);
			super.finish();
		break;
		}
		}
	}
}
