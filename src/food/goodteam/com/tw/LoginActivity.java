package food.goodteam.com.tw;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class LoginActivity extends Activity {
	
	private EditText e_mail,pwd,pwd_second,your_name,your_address,your_phone,your_address_login;
	private String data_address,data_name,data_phone,data_cdate,data_mdate,data_pwd,from_s_id;
	private String[] from_m_id,from_m_number;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		e_mail = (EditText)findViewById(R.id.e_mail);
		pwd = (EditText)findViewById(R.id.pwd);
		pwd_second = (EditText)findViewById(R.id.pwd_second);
		your_name = (EditText)findViewById(R.id.your_name);
		your_address = (EditText)findViewById(R.id.your_address);
		your_address_login = (EditText)findViewById(R.id.your_address_login);
		your_phone = (EditText)findViewById(R.id.your_phone);
		
		Intent intent = getIntent();
		from_m_id = intent.getStringArrayExtra("m_id");
		from_m_number = intent.getStringArrayExtra("m_number");
		from_s_id = intent.getStringExtra("s_id");
		
		e_mail.setOnEditorActionListener(new OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				
				gotoCustomer(1);
				return false;
			}
		});
	}
	
	private void gotoCustomer(int do_case){
		switch(do_case){
		case 1:
		{
			String e_mail_data = e_mail.getText().toString().trim();
			
			Intent intent = new Intent(this,Customer.class); 
			intent.putExtra("do_case", "1"); 
			intent.putExtra("e_mail", e_mail_data);
			startActivityForResult(intent, 0);
			break;
		}
		case 2:
			String e_mail_data = e_mail.getText().toString().trim();
			data_address = your_address.getText().toString().trim();
			data_name = your_name.getText().toString().trim();
			data_pwd = pwd.getText().toString().trim();
			data_phone = your_address.getText().toString().trim();
			Intent intent = new Intent(this,Customer.class); 
			intent.putExtra("do_case", "2"); 
			intent.putExtra("e_mail", e_mail_data);
			intent.putExtra("pwd", data_pwd);
			intent.putExtra("name", data_name);
			intent.putExtra("address", data_address);
			intent.putExtra("phone", data_phone);
			startActivityForResult(intent, 1);
			break;
		}
		
	}
	
	private void gotoSentActivity(){
		Intent intent = new Intent(this,SentActivity.class);
		intent.putExtra("m_id", from_m_id);
		intent.putExtra("m_number", from_m_number);
		intent.putExtra("s_id", from_s_id);
		startActivity(intent);
		finish();
	}

	private void doLogin(){
		pwd.setOnEditorActionListener(new OnEditorActionListener() {
					
					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
						String pwd_data = pwd.getText().toString().trim();							
						if("".equals(pwd_data)){
							String message = "請輸入密碼";
							pwd.setText("");							
							Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
							
						}
						else if(pwd_data.equals(data_pwd)){
							
							pwd.setVisibility(View.INVISIBLE);
							pwd_second.setVisibility(View.INVISIBLE);
							Toast.makeText(LoginActivity.this, "請輸入你現在的地址吧！", Toast.LENGTH_LONG).show();
							your_address_login.setVisibility(View.VISIBLE);
								
							your_address_login.setOnEditorActionListener(new OnEditorActionListener() {
								public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
									
									String e_mail_data = e_mail.getText().toString().trim();
									data_address = your_address_login.getText().toString().trim();
									
									SharedPreferences personalFile = getSharedPreferences("personalFile", MODE_PRIVATE);
									Editor editor = personalFile.edit();
									editor.putString("e_mail", e_mail_data);
									editor.putString("address", data_address);
									editor.putString("name", data_name);
									editor.putString("phone", data_phone);
									editor.putString("cdate", data_cdate);
									editor.putString("mdate", data_mdate);
									editor.commit();
									
									gotoSentActivity();
									
									return false;
								}
							});
							
								
						}
						else{
							String mssage = "密碼輸入錯誤！！";
							pwd.setText("");							
							Toast.makeText(LoginActivity.this, mssage, Toast.LENGTH_SHORT).show();
						}
						
					
						return false;
					}
				});
	}
	
	private void doSignUp(int do_case){
		switch(do_case){
		case 1:{
			pwd_second.setOnEditorActionListener(new OnEditorActionListener() {
				
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					
					String pwd_data = pwd.getText().toString().trim();	
					String pwd_second_data = pwd_second.getText().toString().trim();
					if("".equals(pwd_data) || "".equals(pwd_second_data)){
						String message = "請輸入密碼";
						pwd.setText("");
						pwd_second.setText("");
						Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
						
					}
					else if(pwd_data.equals(pwd_second_data)){
						
						your_name.setVisibility(View.VISIBLE);
						your_address.setVisibility(View.VISIBLE);
						your_phone.setVisibility(View.VISIBLE);
						your_phone.setOnEditorActionListener(new OnEditorActionListener() {
							
							public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
								gotoCustomer(2);
								return false;
							}
						});
						
					}
					else{
						String message = "密碼輸入錯誤";
						pwd.setText("");
						pwd_second.setText("");
						Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
						
					}
					return false;
				}
			});
		break;
		}
		
		case 2:{
			String e_mail_data = e_mail.getText().toString().trim();
			
			SharedPreferences personalFile = getSharedPreferences("personalFile", MODE_PRIVATE);
			Editor editor = personalFile.edit();
			editor.putString("e_mail", e_mail_data);
			editor.putString("address", data_address);
			editor.putString("name", data_name);
			editor.putString("phone", data_phone);
			editor.putString("cdate", data_cdate);
			editor.putString("mdate", data_mdate);
			editor.commit();
			
			gotoSentActivity();
		break;
		}
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
		case 0:
			if(resultCode == RESULT_OK){
				int do_case = Integer.valueOf(data.getStringExtra("do_case"));
				switch(do_case){
				case 1:{
					data_pwd = data.getStringExtra("pwd");
					data_name = data.getStringExtra("name");
					data_phone = data.getStringExtra("phone");
					data_cdate = data.getStringExtra("cdate");
					data_mdate = data.getStringExtra("mdate");
					pwd.setVisibility(View.VISIBLE);					
					doLogin();
				break;
				}
				case 2:{
					pwd.setVisibility(View.VISIBLE);
					pwd_second.setVisibility(View.VISIBLE);
					String message = "看來您還沒註冊過，那我們來註冊一下吧";
					Toast.makeText(this, message, Toast.LENGTH_LONG).show();
					doSignUp(1);
				break;
				}
				case 3:{
					Toast.makeText(this, "請檢查網路連線", Toast.LENGTH_LONG).show();
				break;
				}
				}
			}
			else{
				Toast.makeText(this, "系統錯誤！", Toast.LENGTH_LONG).show();
			}
		break;
		
		case 1:
			if(resultCode == RESULT_OK){
				int do_case = Integer.valueOf(data.getStringExtra("do_case"));
				switch(do_case){
					case 1:{
						data_address = data.getStringExtra("address");
						data_name = data.getStringExtra("name");
						data_phone = data.getStringExtra("phone");
						data_cdate = data.getStringExtra("cdate");
						data_mdate = data.getStringExtra("mdate");
						doSignUp(2);
						break;
					}
				}
			}
		break;
		}
	}
}

