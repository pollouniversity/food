﻿var map;
	function initialize(lat,lng) {
		var mapOptions = {
			center: new google.maps.LatLng(lat,lng),
			zoom: 14,
			disableDefaultUI: true
		};
		map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		addMarker(lat,lng);
	}

	function moveTo(lat, lng){
		var newpos = new google.maps.LatLng(lat, lng);
		var mapOptions = {
		  center: newpos,
		  zoom: 14,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map.panTo(newpos);
	}
	
	function setMarkers(mlat,mlng,mtitle,ms_id) {
		
		var infowindow = new google.maps.InfoWindow({
		  content: mtitle
	  });
		var myLatLng = new google.maps.LatLng(mlat,mlng);
		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
		});
		
		google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);

	  });
		google.maps.event.addListener(marker, 'dblclick', function() {
			ToStore.gotoStore(ms_id,mtitle);
	  });
	}
	
	function addMarker(lat,lng){
		var myLatLng = new google.maps.LatLng(lat,lng);
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
    });
	}
